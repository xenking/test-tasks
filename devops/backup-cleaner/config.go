package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
	"time"

	"github.com/alexflint/go-arg"
	"github.com/kayrus/putty"
	"github.com/kkyr/fig"
	"golang.org/x/crypto/ssh"
)

type LogConfig struct {
	LogLevel string `cfg:"level" default:"debug" arg:"-l" placeholder:"LEVEL"  help:"log level [info, warning, debug, trace, error, fatal]"`
	LogFile  string `cfg:"file" default:"/backup.log" arg:"-f" placeholder:"PATH" help:"path for logging"`
}

type SFTPConfig struct {
	SFTPAddr string `cfg:"addr" validate:"required" arg:"-a" placeholder:"ADDR" help:"sftp address with port [example: 127.0.0.1:22]"`
	SFTPUser string `cfg:"user" default:"root" arg:"-u" placeholder:"USER" help:"sftp user"`

	AuthType       string         `cfg:"auth_type" default:"password" arg:"-t" placeholder:"TYPE" help:"available types [password, pem, putty]"`
	AuthPassphrase string         `cfg:"passphrase" arg:"-p" placeholder:"PASSWORD" help:"used as password"`
	AuthPublicKey  string         `cfg:"pubkey" arg:"-k" placeholder:"PATH" help:"path to private key"`
	AuthMethod     ssh.AuthMethod `arg:"-"`
}

type CleanConfig struct {
	BackupsFolder string        `cfg:"path" validate:"required" arg:"-d" placeholder:"PATH" help:"path to backup folder"`
	TTLInterval   time.Duration `cfg:"interval" default:"168h" arg:"-i" placeholder:"DURATION" help:"time to live in hours [example: 12h]"`
	Extension     string        `cfg:"ext" default:"tar.gz" arg:"-e" placeholder:"EXT" help:"match specific extension"`
}

type Config struct {
	Log   LogConfig   `cfg:"log"`
	SFTP  SFTPConfig  `cfg:"sftp"`
	Clean CleanConfig `cfg:"clean"`
}

var (
	config = &Config{}
)

func initConfig() (err error) {
	var CLI = &struct {
		ConfigFile string `arg:"-c" placeholder:"PATH" help:"path to config file"`
		LogConfig
		SFTPConfig
		CleanConfig
	}{}
	arg.MustParse(CLI)
	config.Log = LogConfig{
		LogLevel: CLI.LogLevel,
		LogFile:  CLI.LogFile,
	}
	config.SFTP = SFTPConfig{
		SFTPAddr:       CLI.SFTPAddr,
		SFTPUser:       CLI.SFTPUser,
		AuthType:       CLI.AuthType,
		AuthPassphrase: CLI.AuthPassphrase,
		AuthPublicKey:  CLI.AuthPublicKey,
	}
	config.Clean = CleanConfig{
		BackupsFolder: CLI.BackupsFolder,
		TTLInterval:   CLI.TTLInterval,
		Extension:     CLI.Extension,
	}
	if CLI.ConfigFile != "" {
		err = newConfig(CLI.ConfigFile, config)
	}
	switch strings.ToLower(config.SFTP.AuthType) {
	case "password":
		config.SFTP.AuthMethod = ssh.Password(config.SFTP.AuthPassphrase)
	case "pem":
		config.SFTP.AuthMethod = AuthPem(config.SFTP.AuthPublicKey, config.SFTP.AuthPassphrase)
	case "putty":
		config.SFTP.AuthMethod = AuthPutty(config.SFTP.AuthPublicKey, config.SFTP.AuthPassphrase)
	default:
		err = fmt.Errorf("unrecognized ssh auth type %s", config.SFTP.AuthType)
	}
	return
}

func newConfig(configPath string, configStruct interface{}) error {
	abs, err := filepath.Abs(configPath)
	if err != nil {
		return err
	}

	err = fig.Load(configStruct,
		fig.File(filepath.Base(abs)),
		fig.Dirs(filepath.Dir(abs)),
		fig.Tag("cfg"),
	)

	return err
}

func AuthPem(file, pass string) ssh.AuthMethod {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		return nil
	}
	key, err := ssh.ParsePrivateKeyWithPassphrase(buffer, []byte(pass))
	if err != nil {
		return nil
	}
	return ssh.PublicKeys(key)
}

func AuthPutty(file, pass string) ssh.AuthMethod {
	var privateKey interface{}

	puttyKey, err := putty.NewFromFile(file)
	if err != nil {
		return nil
	}
	privateKey, err = puttyKey.ParseRawPrivateKey([]byte(pass))
	if err != nil {
		log.Fatal(err)
	}
	signer, err := ssh.NewSignerFromKey(privateKey)
	if err != nil {
		log.Fatal(err)
	}
	method := ssh.PublicKeys(signer)
	return method
}
