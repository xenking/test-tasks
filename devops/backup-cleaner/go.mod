module gitlab.com/xenking/test-tasks/devops/backup-cleaner

go 1.14

require (
	github.com/alexflint/go-arg v1.3.0
	github.com/kayrus/putty v1.0.1
	github.com/kkyr/fig v0.2.0
	github.com/pkg/sftp v1.11.0
	github.com/rs/zerolog v1.19.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
)
