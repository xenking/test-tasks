# Backup-cleaner
Clears the outdated backup archives

### Usage
```bash
Usage: backup-cleaner [--configfile PATH] [--loglevel LEVEL] [--logfile PATH]
 [--sftpaddr ADDR] [--sftpuser USER] [--authtype TYPE]
 [--authpassphrase PASSWORD] [--authpublickey PATH] [--backupsfolder PATH]
 [--ttlinterval DURATION] [--extension EXT]

Options:
  --configfile PATH, -c PATH
                         path to config file
  --loglevel LEVEL, -l LEVEL
                         log level [info, warning, debug, trace, error, fatal] [default: debug]
  --logfile PATH, -f PATH
                         path for logging [default: /backup.log]
  --sftpaddr ADDR, -a ADDR
                         sftp address with port [example: 127.0.0.1:22]
  --sftpuser USER, -u USER
                         sftp user [default: root]
  --authtype TYPE, -t TYPE
                         available types [password, pem, putty] [default: password]
  --authpassphrase PASSWORD, -p PASSWORD
                         used as password
  --authpublickey PATH, -k PATH
                         path to private key
  --backupsfolder PATH, -d PATH
                         path to backup folder
  --ttlinterval DURATION, -i DURATION
                         time to live in hours [example: 12h] [default: 168h]
  --extension EXT, -e EXT
                         match specific extension [default: tar.gz]
  --help, -h             display this help and exit

```
