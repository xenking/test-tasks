package main

import (
	"io"
	"os"
	"strings"
	"time"

	"github.com/pkg/sftp"
	"github.com/rs/zerolog"
	"golang.org/x/crypto/ssh"
)

var Log zerolog.Logger

func initLog(fileWriter io.Writer) {
	consoleWriter := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	multi := zerolog.MultiLevelWriter(consoleWriter, fileWriter)
	Log = zerolog.New(multi).With().Timestamp().Logger()

	level, err := zerolog.ParseLevel(config.Log.LogLevel)
	if err != nil {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
}

func Bod(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, t.Location())
}

func main() {
	if err := initConfig(); err != nil {
		panic(err)
	}

	logFile, err := os.OpenFile(config.Log.LogFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer logFile.Close()

	initLog(logFile)

	sshConfig := &ssh.ClientConfig{
		User:            config.SFTP.SFTPUser,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth: []ssh.AuthMethod{
			config.SFTP.AuthMethod,
		},
	}

	sshConfig.SetDefaults()

	client, err := ssh.Dial("tcp", config.SFTP.SFTPAddr, sshConfig)
	if err != nil {
		Log.Fatal().Err(err).Msg("failed to dial")
	}
	Log.Info().Str("ssh addr", config.SFTP.SFTPAddr).Msg("connected to")

	// open an SFTP session over an existing ssh connection.
	ftp, err := sftp.NewClient(client, sftp.UseFstat(true))
	if err != nil {
		Log.Fatal().Err(err)
	}
	defer ftp.Close()
	Log.Debug().Msg("connected to sftp")

	currTime := Bod(time.Now())
	// walk a directory
	Log.Info().Str("directory", config.Clean.BackupsFolder).Msg("scanning")
	w := ftp.Walk(config.Clean.BackupsFolder)
	for w.Step() {
		if w.Err() != nil {
			Log.Warn().Err(err).Msg("walker error")
			continue
		}
		if !strings.HasSuffix(w.Path(), config.Clean.Extension) {
			continue
		}
		modtime := w.Stat().ModTime()
		if currTime.Sub(modtime) >= config.Clean.TTLInterval {
			err := ftp.Remove(w.Path())
			if err != nil {
				Log.Error().Err(err).Str("file", w.Path()).Msg("failed to delete old")
				continue
			}
			Log.Info().Timestamp().Str("file", w.Path()).Msg("removed old")
		}
	}
}
