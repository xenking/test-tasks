# Static Files
### Deployment
With Docker
```bash
docker build -t static-files:1.0-local .
docker run -it static-files:1.0-local # interactive mode
# or
docker run -d static-files:1.0-local # to run in background
```

With [docker-compose](https://docs.docker.com/compose/)
```bash
docker-compose up
# or
docker-compose up -d # to run in background
``` 

### About
Example to create an atreugo static file server with basic middleware configuration.<br/>
If you want to see subdirecories and their files served, execute commands below.

```bash
mkdir -p nested/one
echo 'Nested Directory' > ./nested/dir.txt
echo 'One More' > ./nested/one/more.txt
```

### Routes:
- `/`
- `/main`
- `/readme`
- `/gitignore`

- `/static/default`
- `/static/middlewares`
- `/static/custom`
- `/static/readme`
- `/static/gitignore`