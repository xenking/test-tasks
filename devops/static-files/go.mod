module gitlab.com/xenking/test-tasks/devops/backup-cleaner

go 1.14

require (
	github.com/savsgio/atreugo/v11 v11.4.1
	github.com/savsgio/go-logger/v2 v2.0.1
)
